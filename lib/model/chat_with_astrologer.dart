import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class AstrologerChatPage extends StatefulWidget {
  @override
  _AstrologerChatPageState createState() => _AstrologerChatPageState();
}

class _AstrologerChatPageState extends State<AstrologerChatPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Chat with Asrologer',
          style: TextStyle(fontSize: 18, color: Colors.white),
        ),
        actions: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 15, 0),
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
                child: Icon(
                  Icons.filter_list,
                  color: Colors.white,
                ),
              ),
              Container(
                child: Icon(
                  Icons.search,
                  size: 25,
                  color: Colors.white,
                ),
                margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
              ),
            ],
          )
        ],
      ),
      body: _PageBody(),
    );
  }
}

class _PageBody extends StatefulWidget {
  @override
  __PageBodyState createState() => __PageBodyState();
}

class __PageBodyState extends State<_PageBody> {
  List<Astrologer> astrologers = [
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true),
    Astrologer(
        "Bharat",
        ['Vedic Astrology', 'Tarot Reading'],
        ['English', 'Hindi', 'Telugu'],
        7,
        50,
        0,
        1312,
        true,
        true,
        true,
        true,
        false,
        true)
  ];

  double _rating;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Container(
            height: 40,
            padding: EdgeInsets.all(5),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        child: Center(
                          child: Text(
                            'Available Balance: ₹0.0',
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        onRechargeClicked(context);
                      },
                      child: Container(
                        margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.green, width: 2),
                            borderRadius: BorderRadius.circular(10)),
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(8, 5, 8, 5),
                          child: Text(
                            'Recharge',
                            style: TextStyle(
                                color: Colors.green,
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
          Expanded(
              child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: astrologers.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      padding: EdgeInsets.all(8),
                      height: 180,
                      child: Card(
                        child: Expanded(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Container(
                                    width: 80,
                                    height: 80,
                                    margin: EdgeInsets.all(14),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              'https://googleflutter.com/sample_image.jpg'),
                                          fit: BoxFit.fill),
                                    ),
                                  ),
                                  RatingBar(
                                    itemSize: 15,
                                    initialRating: 3,
                                    direction: Axis.horizontal,
                                    itemCount: 5,
                                    itemPadding:
                                    EdgeInsets.symmetric(horizontal: 1.0),
                                    itemBuilder: (context, index) {
                                      switch (index) {
                                        case 0:
                                          return Icon(
                                            Icons.star,
                                          );
                                        case 1:
                                          return Icon(
                                            Icons.star,
                                          );
                                        case 2:
                                          return Icon(
                                            Icons.star,
                                          );
                                        case 3:
                                          return Icon(
                                            Icons.star,
                                          );
                                        case 4:
                                          return Icon(
                                            Icons.star,
                                          );
                                        default:
                                          return Container();
                                      }
                                    },
                                    onRatingUpdate: (rating) {
                                      setState(() {
                                        _rating = rating;
                                      });
                                    },
                                  ),

                                  Padding(
                                    padding: EdgeInsets.all(5),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          child: Icon(Icons.person,size: 14,),
                                        ),
                                        Text('12931 total',style: TextStyle(
                                            fontSize: 11
                                        ),)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              Expanded(
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Expanded(
                                              child: Text('Sashi Kashyap ',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold
                                                ),)
                                          ),
                                          Container(padding: EdgeInsets.all(9),
                                            child: Icon(Icons.verified_user,color: Colors.green,),
                                          )
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.fromLTRB(0, 4, 8, 4),
                                            child: Text('Western Astrology, Corporate Astrology'),
                                          )
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            padding: EdgeInsets.fromLTRB(0, 4, 8, 4),
                                            child: Text('Hindi, English, Gujrati, Punjabi'),
                                          )
                                        ],
                                      ),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  padding: EdgeInsets.fromLTRB(0, 4, 8, 4),
                                                  child: Text('Exp: 9 Years'),
                                                ),
                                                Container(
                                                  padding: EdgeInsets.fromLTRB(0, 4, 8, 4),
                                                  child: Text('₹ 39/min'),
                                                )
                                              ],
                                            ),
                                            flex: 1,
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                            decoration: BoxDecoration(
                                                border: Border.all(color: Colors.green, width: 2),
                                                borderRadius: BorderRadius.circular(10)),
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
                                              child: Text(
                                                'Chat',
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }))
        ],
      ),
    );
  }

  void onRechargeClicked(BuildContext context) {}
}

class Astrologer {
  String name;
  List<String> skills;
  List<String> language;
  int experience;
  int ratePerMin;
  int ratings;
  int customerHandled;
  bool isChat;
  bool isCall;
  bool isReport;
  bool isActive;
  bool onWaiting;
  bool isVerified;

  Astrologer(
      this.name,
      this.skills,
      this.language,
      this.experience,
      this.ratePerMin,
      this.ratings,
      this.customerHandled,
      this.isChat,
      this.isCall,
      this.isReport,
      this.isActive,
      this.onWaiting,
      this.isVerified);
}
