import 'package:astro_app/model/Zodiac.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DailyHoroscopePage extends StatefulWidget {
  @override
  _DailyHoroscopePageState createState() => _DailyHoroscopePageState();
}

class _DailyHoroscopePageState extends State<DailyHoroscopePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Daily Horoscope'),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
              margin: EdgeInsets.all(8.0),
              height: 90.0,
              child: ZodiacList()),
          LongCard()
        ],
      ),
//      backgroundColor: Colors.yellow,
    );
  }
}

class ZodiacList extends StatefulWidget {
  @override
  _ZodiacListState createState() => _ZodiacListState();
}

class _ZodiacListState extends State<ZodiacList> {
  List<Zodiac> zodiacs = [
    Zodiac(
      Icons.add_call,
      'Aries',
    ),
    Zodiac(Icons.local_mall, 'Taurus'),
    Zodiac(Icons.receipt, 'Gemini'),
    Zodiac(Icons.report, 'Cancer'),
    Zodiac(Icons.list, 'Leo'),
    Zodiac(Icons.account_balance, 'Virgo'),
    Zodiac(Icons.access_time, 'Libra'),
    Zodiac(Icons.chat, 'Scorpio'),
    Zodiac(Icons.account_circle, 'Sagittarius'),
    Zodiac(Icons.adb, 'Capricorn'),
    Zodiac(Icons.add_location, 'Aquarius'),
    Zodiac(Icons.add_a_photo, 'Pisces')
  ];

  var pressAttention = false;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: zodiacs.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int position) {
          var zodiac = zodiacs[position];
          return Container(
            margin: EdgeInsets.all(3),
            child: RaisedButton(
              onPressed: () => setState(() {
//                pressAttention = !pressAttention;
//            _onZodiacTapped(context, _zodiacName);
              }),
              color: pressAttention ? Colors.blue : Colors.white,
              child: Padding(
                padding: EdgeInsets.all(5.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Center(
                      child: Icon(zodiac.icon),
                    ),
                    Center(
                      child: Text(
                        zodiac.name,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Center(
                      child: Text('21 Mar - 19 Apr'),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class LongCard extends StatefulWidget {
  @override
  _LongCardState createState() => _LongCardState();
}

class _LongCardState extends State<LongCard> {
  List<Description> descList = [
    Description(
        'Profession',
        'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information,'),
    Description(
        'Luck',
        'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information,'),
    Description(
        'Emotions',
        'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information,'),
    Description(
        'Health',
        'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information,'),
    Description(
        'Love',
        'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information,'),
    Description(
        'Travel',
        'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information, '
            'A for better day for getting things done especially when it comes to spreading information,'),
  ];

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
          itemCount: descList.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int position) {
            return Expanded(
              child: Container(
                child: Card(
                  child: Container(
                    padding: EdgeInsets.fromLTRB(0, 15, 0, 12),
                    child: Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            child: Text(
                              descList[position].title,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0),
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Text(descList[position].details),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
    );
  }
}
class Description {
  String title;
  String details;
  Description(this.title, this.details);
}
