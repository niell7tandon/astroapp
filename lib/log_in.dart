import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LoginPageWidget();
  }
}

class LoginPageWidget extends StatefulWidget {
  @override
  _LoginPageWidgetState createState() => _LoginPageWidgetState();
}

class _LoginPageWidgetState extends State<LoginPageWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Login"),
      ),
      body: LoginBody(),
      resizeToAvoidBottomPadding: false
    );
  }
}

class LoginBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Card(
        child: Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              MobileField(),
              PasswordField(),
              ForgotPassword(),
          SignInButton(),
          DontHaveAnAcc(),
//          SignUp()
            ],
          ),
        ),
      ),
    );
  }
}

class MobileField extends StatefulWidget {
  @override
  _MobileFieldState createState() => _MobileFieldState();
}

class _MobileFieldState extends State<MobileField> {
  final mobileFieldController = TextEditingController();

  String MobileNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 20,
            child: Row(
              children: [
                Container(
                  child: Text(
                    "Mobile Number",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18),
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(5, 15, 0, 0),
            height: 30,
            child: Row(
              children: [
                Expanded(
                    child: Container(
                  height: 50,
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: "Enter Mobile Number",
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.yellow))),
                    controller: mobileFieldController,
                    onChanged: (text) {
                      setState(() {
                        MobileNumber = text;
                      });
                    },
                  ),
                )),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    mobileFieldController.dispose();
    super.dispose();
  }
}

class PasswordField extends StatefulWidget {
  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  final passwordFieldController = TextEditingController();
  bool _passwordVisible;
  String Password;

  String passwordToggle;
  String passwordToggleOff = "password_toggle_off96.png";

  @override
  void initState() {
    _passwordVisible = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 20,
              child: Row(
                children: [
                  Container(
                    child: Text(
                      "Password",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(5, 15, 0, 0),
              height: 30,
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    height: 50,
                    child: TextFormField(
                      obscureText: !_passwordVisible,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return "Password Needed";
                        }
                        return null;
                      },
                      onSaved: (String value) {
//                            _setPassword(value);
                      },
                      decoration: InputDecoration(
                          hintText: "Enter Password",
                          suffixIcon: GestureDetector(
                            child: Icon(_passwordVisible
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onLongPress: () {
                              setState(() {
                                _passwordVisible = true;
                              });
                            },
                            onLongPressUp: () {
                              setState(() {
                                _passwordVisible = false;
                              });
                            },
                          ),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.yellow))),
                      controller: passwordFieldController,
                      onChanged: (text) {
                        setState(() {
                          Password = text;
                        });
                      },
                    ),
                  )),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  void dispose() {
    passwordFieldController.dispose();
    super.dispose();
  }
}

class ForgotPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(12, 5, 12, 12),
          child: GestureDetector(
            onTap: (){
              // TODO: Forgot Password module implementation
            },
            child: Text(
              "Forgot password?",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  decoration: TextDecoration.underline),
            ),
          ),
        ),
      ],
    );
  }
}

class SignInButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
      child: Container(
        height: 20,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.yellow,
          borderRadius: BorderRadius.circular(8)
        ),
        child: GestureDetector(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Text("Sign in",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18
                ),),
            ],
          ),
          onTap: (){
            // TODO : Need to implement SignIn 
          },
        )
      )
    );
  }
}

class DontHaveAnAcc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0,50,0,10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: (){
              // TODO : Needed to update Sign up
            },
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text("Don't have an account? Signup",
                style: TextStyle(
                    fontWeight: FontWeight.bold
                ),),
            )
            ,
          )
        ],
      ),
    );
  }
}
